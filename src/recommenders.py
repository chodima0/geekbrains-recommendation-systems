import pandas as pd
import numpy as np

from scipy.sparse import csr_matrix

from implicit.als import AlternatingLeastSquares
from implicit.nearest_neighbours import ItemItemRecommender
from implicit.nearest_neighbours import bm25_weight, tfidf_weight


class MainRecommender:
  """Recommendations, using ALS.
  Input
  -----
  dataset: pd.DataFrame
    User-item dataset.
  """
  
  def __init__(self, dataset, bm25_weighting=False, tfidf_weighting=False):
    self.__set_user_item(dataset)
    self.__set_id_encoding(dataset)
    self.__set_top_items(dataset)
  
    if bm25_weighting:
      self.user_item = bm25_weight(self.user_item).tocsr()
    elif tfidf_weighting:
      self.user_item = tfidf_weight(self.user_item).tocsr()
      
    self.als_model = self.__als_fit()
    self.item_item_model = self.__item_item_fit()
  
  def __set_user_item(self, dataset):
    self.user_item = pd.pivot_table(
      dataset,
      index='user_id',
      columns='item_id',
      values='quantity',
      aggfunc='count',
      fill_value=0
    )
  
    self.user_item = csr_matrix(self.user_item).tocsr()
    self.user_item.dtype = 'float64'
    
  def __set_id_encoding(self, dataset):
    user_ids = dataset['user_id'].unique()
    item_ids = dataset['item_id'].unique()
    
    user_indices = np.arange(user_ids.shape[0])
    item_indices = np.arange(item_ids.shape[0])
    
    self.user_ids_to_indices = dict(zip(user_ids, user_indices))
    self.item_ids_to_indices = dict(zip(item_ids, item_indices))
    self.user_indices_to_ids = dict(zip(user_indices, user_ids))
    self.item_indices_to_ids = dict(zip(item_indices, item_ids))
  
  def __set_top_items(self, dataset):
    self.top_items_by_users = \
      dataset.groupby(['user_id', 'item_id'], as_index=False)['quantity']\
             .count()\
             .sort_values('quantity', ascending=False)
    self.top_items_by_n_transactions = \
      dataset.groupby('item_id', as_index=False)['quantity']\
             .count()\
             .rename(columns={ 'quantity': 'n_transactions' })\
             .sort_values('n_transactions', ascending=False)
    
    self.top_items_by_users = self.top_items_by_users[self.top_items_by_users['item_id'] != 999999]
    self.top_items_by_n_transactions = \
      self.top_items_by_n_transactions[self.top_items_by_n_transactions['item_id'] != 999999]
    
  def __item_item_fit(self):
    item_item_model = ItemItemRecommender(K=1, num_threads=4)
    item_item_model.fit(self.user_item, show_progress=False)
    return item_item_model
  
  def __als_fit(self):
    als_model = AlternatingLeastSquares(
      factors=20, regularization=0.001, iterations=15, num_threads=4
    )
    als_model.fit(self.user_item, show_progress=False)
    return als_model
  
  def __update_id_encoding(self, user_id):
    if user_id not in self.user_ids_to_indices.keys():
      max_user_index = max(self.user_ids_to_indices.values())
      max_user_index += 1
      
      self.user_ids_to_indices.update({ user_id: max_user_index })
      self.user_indices_to_ids.update({ max_user_index: user_id })
  
  def __extend_recommendations(self, recommendations, N):
    N = N - len(recommendations)
    
    if N > 0:
      recommendations.extend(self.top_items_by_n_transactions['item_id'][:N])
    
    return recommendations

  def __get_model_args_kwargs(self, user_index, N):
    model_args = (user_index, self.user_item)
    model_kwargs = {
      'N': N,
      'filter_already_liked_items': False,
      'recalculate_user': False
    }
    if 999999 in self.item_ids_to_indices.keys():
      model_kwargs.update({ 'filter_items': [self.item_ids_to_indices[999999]] })
    
    return model_args, model_kwargs

  def recommend(self, user_id, N, model_type):
    """Returns recommendations for specific user.
    Input
    -----
    user_id: int
      User id.
    N: int
      Quantity of items to recommend.
    model_type: 'als' | 'item_item'
      The model to utilize to get recommendations.
    
    Output
    ------
    list[int]
      Item ids (recommendations).
    """
    assert model_type in ('als', 'item_item'), \
    "Invalid model_type input. Choose between 'als' and 'item_item'!"
    
    self.__update_id_encoding(user_id)
    
    recommendations = None
    user_index = self.user_ids_to_indices[user_id]
    model_args, model_kwargs = self.__get_model_args_kwargs(user_index, N)
    
    if model_type == 'als':
      recommendations = self.als_model.recommend(*model_args, **model_kwargs)
    elif model_type == 'item_item':
      recommendations = self.item_item_model.recommend(*model_args, **model_kwargs)
    
    recommendations = (
      (self.item_indices_to_ids[item_index], influence)\
      for item_index, influence in \
      zip(recommendations[0], recommendations[1])
    )
    recommendations = sorted(recommendations, key=lambda x: x[1], reverse=True)
    recommendations = [recommendation for recommendation, _ in recommendations]
    recommendations = self.__extend_recommendations(recommendations, N=N)
    
    return recommendations
    
  def similar_items_by_items(self, user_id, N):
    """Returns N items that are similar to N items bought by this user.
    Input
    -----
    user_id: int
      User id.
    N: int
      Quantity of items to recommend.
    
    Output
    ------
    list[int]
      Item ids (recommendations).
    """
    def get_similar_item(item_id):
      item_index = self.item_ids_to_indices[item_id]
      item_indices, _ = self.als_model.similar_items(item_index, N=2)
      item_index = item_indices[1]
      item_id = self.item_indices_to_ids[item_index]
      return item_id
    
    self.__update_id_encoding(user_id)
    
    top_user_items = self.top_items_by_users['user_id'] == user_id
    top_user_items = self.top_items_by_users[top_user_items][:N]
    
    recommendations = top_user_items['item_id'].apply(get_similar_item)
    recommendations = recommendations.tolist()
    recommendations = self.__extend_recommendations(recommendations, N=N)
    
    return recommendations
    
  def similar_items_by_users(self, user_id, N):
    """Returns N items that are similar to N items bought by similar users.
    Input
    -----
    user_id: int
      User id.
    N: int
      Quantity of items to recommend.
    
    Output
    ------
    list[int]
      Item ids (recommendations).
    """
    self.__update_id_encoding(user_id)
    
    recommendations = []
    _, model_kwargs = self.__get_model_args_kwargs(0, 1)
    user_index = self.user_ids_to_indices[user_id]
    
    similar_user_indices, _ = self.als_model.similar_users(user_index, N=N)

    for similar_user_index in similar_user_indices:
      similar_user_id = self.user_indices_to_ids[similar_user_index]
      recommendations.extend(
        self.recommend(similar_user_id, N=1, model_type='item_item')
      )
    
    recommendations = self.__extend_recommendations(recommendations, N=N)
    
    return recommendations