import pandas as pd
import numpy as np


def hit_rate(recommended_list, bought_list):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)

    flags = np.isin(bought_list, recommended_list)
    hit_rate = (flags.sum() > 0) * 1
    
    return hit_rate


def hit_rate_at_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    recommend_list = recommended_list[:k]
    flags = np.isin(bought_list, recommended_list)
    hit_rate = (flags.sum() > 0) * 1
    
    return hit_rate

def precision(recommended_list, bought_list):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    flags = np.isin(bought_list, recommended_list)
    
    precision = flags.sum() / len(recommended_list)
    
    return precision


def precision_at_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    bought_list = bought_list
    recommended_list = recommended_list[:k]
    
    flags = np.isin(bought_list, recommended_list)
    precision = flags.sum() / len(recommended_list)
    
    return precision


def money_precision_at_k(recommended_list, bought_list, prices_recommended, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    prices_recommended = np.array(prices_recommended)
    
    recommended_list = recommended_list[:k]
    prices_recommended = prices_recommended[:k]
    flags = np.isin(recommended_list, bought_list)
    
    sum_ = 0
    for i, price_recommend in enumerate(prices_recommended):
        sum_ += price_recommend * flags[i]
    
    precision = sum_ / sum(prices_recommended)
    
    return precision

def recall(recommended_list, bought_list):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    flags = np.isin(bought_list, recommended_list)
    recall = flags.sum() / len(bought_list)
    
    return recall


def recall_at_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    recommended_list = recommended_list[:k]
    
    flags = np.isin(bought_list, recommended_list)
    recall = flags.sum() / len(bought_list)
    
    return recall

def money_recall_at_k(recommended_list, bought_list, prices_recommended, prices_bought, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    prices_recommended = np.array(prices_recommended)
    prices_bought = np.array(prices_bought)
    
    recommended_list = recommended_list[:k]
    prices_recommended = prices_recommended[:k]
    flags = np.isin(bought_list, recommended_list)
    
    sum_ = 0
    for i, price_bought in enumerate(prices_bought):
        sum_ += price_bought * flags[i]
    
    recall = sum_ / sum(prices_recommended)
    
    return recall

def ap_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    flags = np.isin(recommended_list, bought_list)
    if sum(flags) == 0:
        return 0
    
    sum_ = 0
    for i in range(1, k+1):
        if flags[i] == True:
            p_k = precision_at_k(recommended_list, bought_list, k=i)
            sum_ += p_k
            
    result = sum_ / sum(flags)
    
    return result

def mrr_at_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)
    
    recommended_list = recommended_list[:k]
    
    reversed_ranks = []
    for i, bought in enumerate(bought_list, start=1):
        if bought in recommended_list:
            reversed_ranks.append(1 / i)
    
    mrr = np.mean(reversed_ranks)
    
    return mrr

def map_k(recommended_list, bought_list, k=5):
    aps = []
    for recommended_user_list, bought_user_list in zip(recommended_list, bought_list):
        aps.append(ap_k(recommended_user_list, bought_user_list, k=k))
    
    result = sum(aps) / len(aps)
    
    return result

def ndcg_at_k(recommended_list, bought_list, k=5):
    bought_list = np.array(bought_list)
    recommended_list = np.array(recommended_list)

    recommended_list = recommended_list[:k]
    
    flags = np.isin(recommended_list, bought_list)
    
    dcg = 0
    idcg = 0
    for rank, flag in enumerate(flags, start=1):
        if flag == True:
            dcg += 1 if rank <= 2 else 1 / np.log2(rank)
        idcg += 1 if rank <= 2 else 1 / np.log2(rank)
    
    ndcg = dcg / idcg
    
    return ndcg