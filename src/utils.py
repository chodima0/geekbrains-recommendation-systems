import numpy as np
import pandas as pd



def prefilter_items(data, take_n_popular=5000, item_features=None):
  # Remove the most popular items by n unique users of each item
  data_by_popularity = data.groupby('item_id', as_index=False)['user_id']\
                           .nunique()\
                           .rename(columns={ 'user_id': 'n_users' })\
                           .sort_values('n_users', ascending=False)
                                      
  threshold_mean = data_by_popularity['n_users'].mean().round()
  data_by_non_popularity = data_by_popularity[data_by_popularity['n_users'] < threshold_mean]
  data = data[data['item_id'] & data_by_non_popularity['item_id']]
  
  # Remove the least popular items by n unique users of each item
  data_by_non_popularity = data_by_non_popularity[data_by_non_popularity['n_users'] != 0]
  data = data[data['item_id'] & data_by_non_popularity['item_id']]
  
  # Remove items that where not on sale last 12 monthes
  max_week = data['week_no'].max()
  if max_week > 18:
    left_data = data[data['week_no'] < max_week - 12]
    right_data = data[data['week_no'] >= max_week - 12]
    
    left_data = left_data[left_data['item_id'] & right['item_id']]
    data = data[data['item_id'] & left_data['item_id']]
  
  # Remove the least popular items by its department
  if item_features is not None:
    department_data = item_features.groupby('department', as_index=False)['item_id']\
                                   .nunique()\
                                   .rename(columns={'item_id': 'n_items'})\
                                   .sort_values('n_items', ascending=False)
    department_data = department_data[department_data['n_items'] > 150]
    item_features = item_features[item_features['department'].isin(department_data['department'])]
    data = data[data['item_id'] & item_features['item_id']]
  
  # Remove the cheapest items
  data['price'] = data['sales_value'] * data['quantity']
  data = data[data['price'] > 2]
  
  # Remove the most expensive items
  data = data[data['price'] < 50]
  
  # Take currently the most popular items
  data_by_popularity = data.groupby('item_id', as_index=False)['quantity']\
                           .sum()\
                           .rename(columns={ 'quantity': 'n_sold' })\
                           .sort_values('n_sold', ascending=False)\
                           .head(take_n_popular)
  data = data[data['item_id'] & data_by_popularity['item_id']]
  
  # Create fictitious items
  data.loc[~(data['item_id'] & data_by_popularity['item_id']), 'item_id'] = 999999
  
  return data